<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument()
 */
class ProductGroupRef {

    /**
     * @var ProductGroup
     * @MongoDB\ReferenceOne(targetDocument="ProductGroup", storeAs="dbRef")
     */
    private $group;

    /**
     * @var array materialized paths
     * @MongoDB\Field(type="collection")
     */
    private $paths;

    /**
     * @return ProductGroup
     */
    public function getGroup(): ProductGroup {
        return $this->group;
    }

    /**
     * @param ProductGroup $group
     */
    public function setGroup( ProductGroup $group ): void {
        $this->group = $group;
    }

    /**
     * @return array
     */
    public function getPaths(): array {
        return $this->paths;
    }

    /**
     * @param array $paths
     */
    public function setPaths( array $paths ): void {
        $this->paths = $paths;
    }

}
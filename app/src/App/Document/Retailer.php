<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="retailer")
 *   @MongoDB\Indexes({
 *      @MongoDB\Index(keys={"oldId"="asc"})
 * })
 */
class Retailer {

    /**
     * @var string
     * @MongoDB\Id(strategy="NONE", type="string")
     */
    private $id;

    /**
     * @MongoDB\Field(type="integer")
     * @var int
     */
    private $oldId;

    /**
     * @MongoDB\Field(type="string")
     * @var string $name
     */
    private $name;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="App\Document\Asset")
     */
    private $assets;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="App\Document\RetailerDetail")
     */
    private $details;

    /**
     * @var Meta
     * @MongoDB\EmbedOne(targetDocument="App\Document\Meta")
     */
    private $meta;

    //private $groups;

    /**
     * Retailer constructor.
     */
    public function __construct() {
        $this->assets = new ArrayCollection([]);
        $this->details = new ArrayCollection([]);
        $this->meta = new Meta();
    }

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId( string $id ): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getAssets(): Collection {
        return $this->assets;
    }

    /**
     * @param Collection $assets
     */
    public function setAssets( Collection $assets ): void {
        $this->assets = $assets;
    }

    public function addAsset( Asset $asset ): void {
        $this->assets->add($asset);
    }
    /**
     * @return Collection
     */
    public function getDetails(): Collection {
        return $this->details;
    }

    /**
     * @param Collection $details
     */
    public function setDetails( Collection $details ): void {
        $this->details = $details;
    }

    public function addDetail( RetailerDetail $detail) : void {
        $this->details->add($detail);
    }

    /**
     * @return Meta
     */
    public function getMeta(): Meta {
        return $this->meta;
    }

    /**
     * @param Meta $meta
     */
    public function setMeta( Meta $meta ): void {
        $this->meta = $meta;
    }

}
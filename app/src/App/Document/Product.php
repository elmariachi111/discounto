<?php

namespace App\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="product", repositoryClass="App\Repository\ProductRepository")
 */
class Product {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $shortText;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $longText;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="Offer")
     */
    private $offers;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="App\Document\Asset")
     */
    private $assets;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="App\Document\ProductGroupRef")
     */
    private $groups;

    /**
     * Product constructor.
     */
    public function __construct() {
        $this->offers = new ArrayCollection;
        $this->assets = new ArrayCollection;
        $this->groups = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId( $id ): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getShortText(): string {
        return $this->shortText;
    }

    /**
     * @param string $shortText
     */
    public function setShortText( string $shortText ): void {
        $this->shortText = $shortText;
    }

    /**
     * @return string
     */
    public function getLongText(): string {
        return $this->longText;
    }

    /**
     * @param string $longText
     */
    public function setLongText( string $longText ): void {
        $this->longText = $longText;
    }

    /**
     * @return Collection
     */
    public function getOffers(): Collection {
        return $this->offers;
    }

    /**
     * @param Collection $offers
     */
    public function setOffers( Collection $offers ): void {
        $this->offers = $offers;
    }

    /**
     * @return Collection
     */
    public function getAssets(): Collection {
        return $this->assets;
    }

    /**
     * @param Collection $assets
     */
    public function setAssets( Collection $assets ): void {
        $this->assets = $assets;
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     */
    public function setGroups( Collection $groups ): void {
        $this->groups = $groups;
    }

    /**
     * @return Retailer
     */
    public function getRetailer() {
        return $this->getOffers()->first()->getRetailer();
    }

}
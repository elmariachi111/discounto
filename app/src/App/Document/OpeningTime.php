<?php


namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument()
 */
class OpeningTime {

    /**
     * @var string
     * @MongoDB\Field(type="int", name="sd")
     */
    private $startDay;

    /**
     * @var string
     * @MongoDB\Field(type="int", name="ed")
     */
    private $endDay;

    /**
     * @var int
     * @MongoDB\Field(type="int", name="om")
     */
    private $openingMinutes;

    /**
     * @var int
     * @MongoDB\Field(type="int", name="cm")
     */
    private $closingMinutes;


    /**
     * @return string
     */
    public function getStartDay(): string {
        return $this->startDay;
    }

    /**
     * @param string $startDay
     */
    public function setStartDay( string $startDay ): void {
        $this->startDay = $startDay;
    }

    /**
     * @return string
     */
    public function getEndDay(): string {
        return $this->endDay;
    }

    /**
     * @param string $endDay
     */
    public function setEndDay( string $endDay ): void {
        $this->endDay = $endDay;
    }

    /**
     * @return int
     */
    public function getOpeningMinutes(): int {
        return $this->openingMinutes;
    }

    /**
     * @param int $openingMinutes
     */
    public function setOpeningMinutes( int $openingMinutes ): void {
        $this->openingMinutes = $openingMinutes;
    }

    /**
     * @return int
     */
    public function getClosingMinutes(): int {
        return $this->closingMinutes;
    }

    /**
     * @param int $closingMinutes
     */
    public function setClosingMinutes( int $closingMinutes ): void {
        $this->closingMinutes = $closingMinutes;
    }



}
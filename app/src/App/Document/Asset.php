<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

//, repositoryClass="CrmBundle\Document\Repository\CustomerRepository"

/**
 * @MongoDB\EmbeddedDocument()
 */
class Asset {

    const ICON      = 'icon';
    const LIST      = 'list';
    const LISTBLOCK = 'listblock';
    const DETAIL    = 'detail';
    const XXL       = 'xxl';
    const ORIGINAL  = 'original';

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $type;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $width;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $height;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $path;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldId;

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType( string $type ): void {
        $this->type = $type;
    }


    /**
     * @return int
     */
    public function getWidth(): int {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth( int $width ): void {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight( int $height ): void {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath( string $path ): void {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getOldId(): ?int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }


}
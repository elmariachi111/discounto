<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="stores", repositoryClass="App\Repository\StoreRepository")
 * @MongoDB\Index(keys={"coordinates"="2dsphere"})
 */
class Store {
    /**
     * @var
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldId;

    /** @var Coordinates
     * @MongoDB\EmbedOne(targetDocument="Coordinates")
     */
    private $coordinates;

    /**
     * @var Address
     * @MongoDB\EmbedOne(targetDocument="Address")
     */
    protected $address;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $phoneNumber;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $description;

    /**
     * @var Retailer
     * @MongoDB\ReferenceOne(targetDocument="Retailer", storeAs="dbRef")
     */
    private $retailer;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $openingTimeString;

    /**
     * @var Collection<OpeningTime>
     * @MongoDB\EmbedMany(targetDocument="OpeningTime")
     */
    private $openingTimes;

    /**
     * Store constructor.
     */
    public function __construct() {
        $this->openingTimes = new ArrayCollection;
    }


    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId( $id ): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates(): Coordinates {
        return $this->coordinates;
    }

    /**
     * @param Coordinates $coordinates
     */
    public function setCoordinates( Coordinates $coordinates ): void {
        $this->coordinates = $coordinates;
    }

    /**
     * @return Retailer
     */
    public function getRetailer(): Retailer {
        return $this->retailer;
    }

    /**
     * @param Retailer $retailer
     */
    public function setRetailer( Retailer $retailer ): void {
        $this->retailer = $retailer;
    }

    /**
     * @return string
     */
    public function getOpeningTimeString(): string {
        return $this->openingTimeString;
    }

    /**
     * @param string $openingTimeString
     */
    public function setOpeningTimeString( string $openingTimeString ): void {
        $this->openingTimeString = $openingTimeString;
    }

    /**
     * @return Collection
     */
    public function getOpeningTimes(): Collection {
        return $this->openingTimes;
    }

    /**
     * @param Collection $openingTimes
     */
    public function setOpeningTimes( Collection $openingTimes ): void {
        $this->openingTimes = $openingTimes;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }


    /**
     * @return string
     */
    public function getPhoneNumber(): string {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber( string $phoneNumber ): void {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( string $description ): void {
        $this->description = $description;
    }



}
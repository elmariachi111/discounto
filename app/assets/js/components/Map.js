/* global window */
import React, { Children } from 'react'

import {Component} from 'react';
import ReactMapGL, {Marker, NavigationControl, Popup} from 'react-map-gl';
import axios from 'axios';
import env from '../env';




class MapMarker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...props
        };
    }

    render() {
        const retailer = this.props.retailer;
        const pin = retailer.assets.filter(a => a.type === 'icon')[0];
        const imgurl = env.IMAGE_ROOT + pin.path + pin.name;
        return <Marker longitude={this.props.store.coordinates.lon} latitude={this.props.store.coordinates.lat}
                       captureClick={true}
        >
            <img src={imgurl} onClick={this.props.onClick}/>
        </Marker>
    }
}

export default class Map extends Component {

    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                width: 600,
                height: 400,
                latitude: props.lat,
                longitude: props.lon,
                zoom: 12
            },
            mapStyle: {

            },
            stores: [],
            retailers: {},
            selected: {
                store: null,
                retailer: null
            }

        };
        this.loadStores = this.loadStores.bind(this);
        this.onViewPortChange = this.onViewPortChange.bind(this);
    }

    loadStores() {
        const zoom = this.state.viewport.zoom;
        let radius;
        if (zoom < 8) {
            radius = 10000;
        } else if(zoom < 11) {
            radius = 5000;
        } else {
            radius = 2500;
        }

        const params = {
            lat: this.state.viewport.latitude,
            lon: this.state.viewport.longitude,
            radius: radius
        };

        axios.get(`${env.ROOT_URL}/stores`, {params})
            .then(res => {
                const retailers = {};
                const stores = [];
                res.data.forEach(rws => {
                    retailers[rws.retailer.id] = rws.retailer;
                    stores.push(...rws.stores);
                });
                this.setState({ stores, retailers });
            });
    }
    componentDidMount() {
        window.addEventListener('resize', this._resize.bind(this));
        this._resize();

    }

    _resize() {
        this.onViewPortChange({
            width: this.props.width || window.innerWidth,
            height: this.props.height || 500
        });
    }

    onViewPortChange(viewport) {
        this.setState({
            viewport: {...this.state.viewport, ...viewport}
        })
    }

    render() {

        const {viewport, mapStyle} = this.state;

        const coords = [13.423908, 52.492142];

        const markers = this.state.stores.map(store =>
            <MapMarker store={store} retailer={this.state.retailers[store.retailer]} key={store.id} onClick={() => {
                const selected = {
                    store,
                    retailer: this.state.retailers[store.retailer]
                };
                this.setState({ selected });
                this.props.onRetailerSelected(selected.retailer);
            } } />
        );

        return (
            <div>
                <ReactMapGL
                {...viewport}
                mapboxApiAccessToken={env.MAPBOX_TOKEN}
                onViewportChange={this.onViewPortChange}
            >
                    <div style={{position: 'absolute', right: 0}}>
                        <NavigationControl  />
                    </div>

                    {markers}

                    {this.state.selected.store && (
                        <Popup tipSize={5}
                               anchor="bottom-right"
                               longitude={this.state.selected.store.coordinates.lon}
                               latitude={this.state.selected.store.coordinates.lat}
                               onClose={() => this.setState({selected: {store: null, retailer: null} })}
                               closeOnClick={true}>
                            <p>
                                {this.state.selected.retailer.name}
                            </p>
                            <p className="is-bold">{this.state.selected.store.address.street} {this.state.selected.store.address.streetnumber}</p>
                            <p>{this.state.selected.store.address.zip} {this.state.selected.store.address.city}</p>
                        </Popup>
                    )}

                </ReactMapGL>

                <button className="button" onClick={this.loadStores}>Search here</button>

            </div>
    );
    }
}
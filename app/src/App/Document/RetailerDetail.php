<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

//, repositoryClass="CrmBundle\Document\Repository\CustomerRepository"

/**
 * @MongoDB\EmbeddedDocument()
 */
class RetailerDetail {

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $language;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $teaser;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $description;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $seoText;

    /**
     * @return string
     */
    public function getLanguage(): string {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage( string $language ): void {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getTeaser(): string {
        return $this->teaser;
    }

    /**
     * @param string $teaser
     */
    public function setTeaser( ?string $teaser ): void {
        $this->teaser = $teaser;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( ?string $description ): void {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSeoText(): string {
        return $this->seoText;
    }

    /**
     * @param string $seoText
     */
    public function setSeoText( ?string $seoText ): void {
        $this->seoText = $seoText;
    }




}
import React, {Children, Component} from 'react'
import axios from 'axios/index';
import env from '../env';
import currency from 'currency.js';

const ProductBox = ({product}) => {
    const asset= product.assets.filter(a => a.type === 'xxl' || a.type === 'detail')[0];
    const imgurl = env.IMAGE_ROOT + asset.path + asset.name;

    return <div className="box">
        <article className="media">
            <div className="media-left">
                <figure className="image is-64x64">
                    <img src={imgurl} alt="Image" />
                </figure>
            </div>
            <div className="media-content">
                <div className="content">
                    <p className="title  is-5">{product.name}</p>
                    <p>{product.short_text}</p>
                </div>

                <span className="is-small">
                  <p>€{currency(product.offer.price/100).format()}</p>
                </span>
            </div>
        </article>
    </div>
};

export default class Offers extends Component {

    constructor(props) {
        super(props);

        this.state = {
            products: []
        }
    }

    loadOffers() {
        const params = {
            retailer: this.props.retailer.id,
            date: this.props.day.toFormat('yyyy-LL-dd')
        };

        axios.get(`${env.ROOT_URL}/offers`, {params})
            .then(res => {
                this.setState({ products: res.data.products || [] });
            });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!this.props.retailer)
            return;

        const retailerSame = (prevProps.retailer && prevProps.retailer.id === this.props.retailer.id);
        if (retailerSame)
            return;

        //const dateSame= (prevProps.day.hasSame(this.props.date, 'day'));

        /*if (retailerSame && dateSame)
            return;
*/
        this.loadOffers();
    }

    render() {

        const offers = this.state.products.map( product => (
            <div className="column is-one-third"  key={product.id}>
                <ProductBox product={product} />
            </div>
        ));

        return <div>

            <section className="section">
                <div className="container">
                    {this.props.retailer &&
                    <h1 className="title">
                        {this.props.retailer.name}
                        {this.props.day && ' am ' + this.props.day.toISODate() }
                    </h1>
                    }
                </div>
                <div className="columns is-multiline">
                    {offers}
                </div>

            </section>

        </div>
    }
}
